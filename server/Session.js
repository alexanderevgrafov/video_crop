"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Session = void 0;
const type_r_1 = require("type-r");
let Session = class Session extends type_r_1.Record {
    constructor() {
        super(...arguments);
        this.log = (...params) => this.dolog('log', ...params);
        this.log_error = (...params) => this.dolog('error', ...params);
    }
    dolog(level, ...params) {
        console[level]('[' + (this.client_name || '---') + ']' + (level !== 'log' ? ('[' + level + ']') : '') + ': ', ...params);
    }
    emit(name, ...data) {
        this.socket && this.socket.emit(name, ...data);
    }
    ws_progress(input, output) {
        this.emit('progress', { __status: 'ok', __sig: input.signature, progress: output });
        this.log('Progress emitted: ', output);
    }
    ws_success(input, output) {
        this.emit('answer', { __status: 'ok', __sig: input.signature, answer: output });
        this.log('Answer emitted: ', output);
    }
    ws_error(input, output) {
        this.emit('answer', { __status: 'error', __sig: input.signature, msg: output.message });
        this.log_error('Error emitted: ', output.message || output);
    }
};
__decorate([
    type_r_1.auto,
    __metadata("design:type", String)
], Session.prototype, "client_name", void 0);
__decorate([
    type_r_1.auto,
    __metadata("design:type", Object)
], Session.prototype, "socket", void 0);
__decorate([
    type_r_1.auto,
    __metadata("design:type", String)
], Session.prototype, "fileName", void 0);
__decorate([
    type_r_1.auto,
    __metadata("design:type", Function)
], Session.prototype, "progress", void 0);
Session = __decorate([
    type_r_1.define
], Session);
exports.Session = Session;
//# sourceMappingURL=Session.js.map