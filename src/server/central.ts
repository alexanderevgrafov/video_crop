import * as _ from 'lodash'

_.extend(process.env, require('dotenv').config().parsed || {});

import 'reflect-metadata'
import * as socketio from 'socket.io'
import {logger} from 'type-r'
import {api_map, createMediaFolders} from './socket_api'
import {Session} from "./Session";

const {exec} = require('child_process');
const fastify = require('fastify')({logger: {level: 'error'}});
const path = require('path');
const OBSWebSocket = require('obs-websocket-js');

const ws = socketio.listen(process.env.WS_SERVER_PORT);
let obs_socket = null;

console.log('SocketIO server is listening on', process.env.WS_SERVER_PORT);

function DoObsConnect(timeout = 1) {
    const obs = new OBSWebSocket();
    obs.connect({address: process.env.OBS_SOCKET_SERVER, password: process.env.OBS_SOCKET_SERVER_PASSWORD})
        .then(() => {
            console.log('OBS is connected');
            obs_socket = obs;

            obs.on('SwitchScenes', data => {
                console.log(`OBS scene: ${data.sceneName}`);
            });

            obs.on('ConnectionClosed', () => {
                console.error('OBS connection closed');
                obs_socket = null;
                DoObsConnect();
            });

        })
        .catch(err => {
            if (timeout < 600) {
                console.error('OBS connection retry after', timeout * 2, 'sec');
                setTimeout(() => DoObsConnect(timeout * 2), timeout * 1000);
            } else {
                console.error('OBS connection attempts canceled', err.description);
            }
        });
}

const errOut = err => console.error(err);

ws.on('connection', socket => {
    const session = new Session({socket});

    console.log('Socket IO client is connected');
    _.map(api_map, (method, name) =>
        socket.on(name, data => {
                try {
                    session.progress = dt => session.ws_progress(data, dt);

                    const result = method(session, data.params);

                    result && result.then && result
                        .then(obj => session.ws_success(data, obj))
                        .catch((err: Error) => {
                            session.ws_error(data, err);
                        })
                } catch (err) {
                    session.ws_error(data, err);
                }
            }
        ));
});

DoObsConnect();
let screen_capture = false;

fastify
    .register(require('fastify-static'), {
        root: path.join(__dirname, '/../public'),
    })
    .get(process.env.SCREEN_CAPTURE_URL,
        {
            schema: {
                query: {
                    [process.env.SCREEN_CAPTURE_PARAM_NAME]: {type: 'string'},
                },
                response: {
                    200: {
                        type: 'object',
                        properties: {
                            status: {type: 'string'}
                        }
                    }
                }
            },
            handler: function (request, reply) {
                const command = request.query[process.env.SCREEN_CAPTURE_PARAM_NAME];
                let vlc_command = '';

                if (obs_socket) {
                    if (command === process.env.SCREEN_CAPTURE_VALUE_START) {
                        obs_socket.send('SetCurrentScene', {
                            'scene-name': process.env.OBS_SCENE_NAME_CAPTURE
                        }).catch(errOut);
                        obs_socket.send('StartRecording').catch(errOut);
                        vlc_command = process.env.VLC_CONTROL_CMD_ON;
                        screen_capture = true;
                    }
                    if (command === process.env.SCREEN_CAPTURE_VALUE_STOP) {
                        obs_socket.send('StopRecording').catch(errOut);
                        obs_socket.send('SetCurrentScene', {
                            'scene-name': process.env.OBS_SCENE_NAME_STOP
                        }).catch(errOut);
                        vlc_command = process.env.VLC_CONTROL_CMD_OFF;
                        screen_capture = false;
                    }

                    if (vlc_command) {
                        console.log('VLC command:', vlc_command);

                        exec(vlc_command,
                            (err, stdout, stderr) => {
                                if (err) {
                                    console.log('Exec VLC command error', err);
                                    return;
                                }
                            })
                    }
                } else {
                    console.error(`No OBS connection found. '${command}' command is skipped.`);
                }
                reply.send({status: screen_capture ? 'on' : 'off'})
            }
        })
    .listen(process.env.SERVER_PORT, '0.0.0.0', (err, address) => {
        if (err) throw err;
        console.log(`Web server is on ${address}`);
    });

logger.off();

createMediaFolders();
