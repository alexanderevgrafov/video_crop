const _ = require('lodash');
const fs = require('fs');
const util = require('util');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
const { getVideoDurationInSeconds } = require('get-video-duration');
const through2 = require('through2');
const SLASH = process.env.SYSTEM_SLASH || '/';
const publicFolder = __dirname + SLASH + process.env.PUBLIC_FOLDER + SLASH;

ffmpeg.setFfmpegPath(ffmpegPath);

import {WsHandler} from './interfaces'

const disconnect = s => s.log('WS client is disconnected');

const hola: WsHandler = (s, data: string) => {
    s.log('Hola from', data);
    s.client_name = data;

    clearOlderFiles();

    return Promise.resolve('Hellow');
};

function preparePublicPath(path) {
    return path.split(process.env.PUBLIC_FOLDER).pop();
}

function uploadFile(s, dt) {
    const {appendTo, fileName, fileData} = dt;
    const store_dir = publicFolder + process.env.STORAGE_FOLDER + SLASH;
    let fileNameNew;

    if (!appendTo) {
        const momentMark = (new Date).getTime();
        const fileNameArr = _.map((fileName || 'data').split('.'), str => str.substr(0, 10));
        const fileExt = fileNameArr.pop();

        fileNameNew = fileNameArr.join('.') + '_' + momentMark + '.' + fileExt;
    } else {
        fileNameNew = appendTo;
    }

    return util.promisify(fs.writeFile)(store_dir + fileNameNew, fileData, {flag: 'a+'})
        .then(() => {
            s.fileName = fileNameNew;
            return {isUploaded: true, appendTo: fileNameNew};
        });
}

function progressSeconds(prog){
        const sp = prog.timemark.split(":");
        let seconds = 0;
        if (typeof(sp) == "undefined") {
            seconds = prog.timemark;
        } else {
            if (typeof(sp[3]) != "undefined") {
                seconds = parseInt(sp[0]) * 24 * 60 * 60 + parseInt(sp[1]) * 60 * 60 +
                    parseInt(sp[2]) * 60 + parseInt(sp[3]);
            } else if (typeof(sp[2]) != "undefined") {
                seconds = parseInt(sp[0]) * 60 * 60 + parseInt(sp[1]) * 60 +
                    parseInt(sp[2]);
            } else if (typeof(sp[1]) != "undefined") {
                seconds = parseInt(sp[0]) * 60 + parseInt(sp[1]);
            } else if (typeof(sp[0]) != "undefined") {
                seconds = parseInt(sp[0]);
            }
        }

        return seconds;
}

function autoProcess(s, dt) {
    const SX = parseInt(process.env.VIDEO_WIDTH) || 300;
    const SY = parseInt(process.env.VIDEO_HEIGHT) || 300;
    const store_dir = publicFolder + process.env.STORAGE_FOLDER + SLASH;
    const processed_dir = publicFolder + process.env.PROCESSED_FOLDER + SLASH;

    return new Promise(resolve => {
        getVideoDurationInSeconds(store_dir + s.fileName)
            .then((duration) => {
                ffmpeg(store_dir + s.fileName)
                    .noAudio()
                    .complexFilter([
                        'scale=w=' + SX + ':h=' + SY + ':force_original_aspect_ratio=decrease[rescaled]',
                        {
                            filter: 'pad', options: SX + ':' + SY + ':(ow-iw)/2:(oh-ih)/2',
                            inputs: 'rescaled', outputs: ['out']
                        },
                    ], 'out')
                    .on('progress', progress => {
                        s.progress({percent: progressSeconds(progress) / duration})
                    })
                    .on('end', () => resolve({
                        resultFile: preparePublicPath(processed_dir + s.fileName)
                    }))
                    .save(processed_dir + s.fileName);
            });
    });
}

function cropProcess(s, dt) {
    const SX = parseInt(process.env.VIDEO_WIDTH) || 300;
    const SY = parseInt(process.env.VIDEO_HEIGHT) || 300;
    const store_dir = publicFolder + process.env.STORAGE_FOLDER + SLASH;
    const processed_dir = publicFolder + process.env.PROCESSED_FOLDER + SLASH;
    const {crop} = dt;
    const scale = 'ih/' + process.env.CROPPER_HEIGHT;
    const filterLine = `-filter:v crop=${crop.width}*${scale}:${crop.height}*${scale}:${crop.left}*${scale}:${crop.top}*${scale},scale=${SX}x${SY}`;
    const inputFile = store_dir + s.fileName;

    console.log('INPUT: ', inputFile);

    return new Promise(resolve => {
        getVideoDurationInSeconds(store_dir + s.fileName)
            .then((duration) => {
                ffmpeg(inputFile)
                    .noAudio()
                    .outputOptions(filterLine)
                    .on('progress', progress => {
                        s.progress({percent: progressSeconds(progress) / duration})
                    })

                    .on('end', function () {
                        resolve({
                            resultFile: preparePublicPath(processed_dir + s.fileName)
                        })
                    })
                    .save(processed_dir + s.fileName);
            });
    });
}

function getThumbs(s, dt) {
    const store_dir = publicFolder + process.env.STORAGE_FOLDER + SLASH;
    let bufferString = '';

    return new Promise(resolve => {
        ffmpeg(store_dir + s.fileName)
            .noAudio()
            .size('?x' + process.env.CROPPER_HEIGHT)
            .frames(1)
            .format('singlejpeg')
            .pipe(through2(
                (chunk, enc, cb) => {
                    bufferString += chunk.toString('base64');
                    cb();
                },
                cb => { // flush function
                    cb();
                    resolve({buffer: bufferString});
                }
            ), {end: true});
    });
}

function clearDirectory(directoryPath){
    const now = (new Date()).getTime()/1000;

    fs.readdir(directoryPath, function (err, files) {
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }

        files.forEach(function (fileName) {
            if (fileName[0]!=='.') {
                const file = directoryPath + SLASH + fileName;
                const stats = fs.statSync(file);
                const fileTime = stats.ctimeMs/1000;


                if (!stats.isDirectory() && now - parseInt(process.env.KEEP_FILES_TIMEOUT) > fileTime) {
                    fs.unlinkSync(file);
                }
            }
        });
    });
}

function clearOlderFiles(){
    clearDirectory(publicFolder + process.env.STORAGE_FOLDER);
    clearDirectory(publicFolder + process.env.PROCESSED_FOLDER);
    clearDirectory(publicFolder + process.env.THUMBS_FOLDER);
}

export function createMediaFolders() {
    const store_dir = publicFolder + process.env.STORAGE_FOLDER;

    if (!fs.existsSync(store_dir)) {
        fs.mkdirSync(store_dir, {mode: 0o744, recursive: true});
        fs.mkdirSync(publicFolder + process.env.PROCESSED_FOLDER, {mode: 0o744, recursive: true});
        fs.mkdirSync(publicFolder + process.env.THUMBS_FOLDER, {mode: 0o744, recursive: true});

        console.log('All necessary media folders are created successfully!');
    }

    console.log('Storage is at', store_dir);
}

export const api_map = {
    hola,
    disconnect,
    upload: uploadFile,
    autoProcess,
    cropProcess,
    getThumbs,
};
