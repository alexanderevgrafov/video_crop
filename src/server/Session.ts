import {Record, define, auto} from 'type-r'
import { WsInputObject } from './interfaces'

@define
export class Session extends Record {
    @auto client_name: string;
    @auto socket: any;
    @auto fileName: string;

    @auto progress: Function;

    log = (...params) => this.dolog('log', ...params);
    log_error = (...params) => this.dolog('error', ...params);

    private dolog(level: string, ...params) {
        console[level]('[' + (this.client_name || '---') + ']' + (level !== 'log' ? ('[' + level + ']') : '') + ': ', ...params);
    }

    emit(name: string, ...data) {
        this.socket && this.socket.emit(name, ...data);
    }

    ws_progress(input: WsInputObject, output: any) {
        this.emit('progress', {__status: 'ok', __sig: input.signature, progress: output});
        this.log('Progress emitted: ', output)
    }

    ws_success(input: WsInputObject, output: any) {
        this.emit('answer', {__status: 'ok', __sig: input.signature, answer: output});
        this.log('Answer emitted: ', output)
    }

    ws_error(input: WsInputObject, output: Error) {
        this.emit('answer', {__status: 'error', __sig: input.signature, msg: output.message});
        this.log_error('Error emitted: ', output.message || output);
    }
}
