import React from 'react-mvx'
import { Events }       from 'type-r'
import 'lib/notify'
import 'lib/notify.css'

const Page = {
    globalState : null,   // Common state available for all pages
    msgHash     : {},
    isResizing  : false,

    forceResize() {
        if( !this.isResizing ) {
            this.isResizing = true;
            try {
                this.trigger( 'page-resize' );
            }
            finally {
                this.isResizing = false;
            }
        }
    },

    notifyOnComplete( promise, { error, success, before } ) {
        const beforeMsg = before && this.createMsg( {
            text     : before,
            duration : 0,
            type     : 'notify'
        } );

        return promise.then( () => {
            this.createMsg( {
                text    : success,
                type    : 'success',
                replace : beforeMsg.uuid
            } )
        } )[ promise.catch ? 'catch' : 'fail' ](
            err => {
                this.ioFailMsg( err, { errorDescription : error, replace : beforeMsg.uuid } )
            } );
    },

    superNotify( promise, { error, success, before } ) {
        let beforeMsg = before && this.createMsg( {
            text     : before,
            duration : 0,
            type     : 'notify'
        } );

        const progress = progress => {
            if (before && beforeMsg) {
                beforeMsg.element.text( before + ' ' + Math.round(progress*100) + '%');
            }
        };

        const prom = promise.then( () => {
            if (success) {
                this.createMsg({
                    text: success,
                    type: 'success',
                    replace: beforeMsg.uuid
                })
            } else {
                this.removeMsg(beforeMsg.uuid);
            }
        } )[ promise.catch ? 'catch' : 'fail' ](
          err => this.ioFailMsg( err, { errorDescription : error, replace : beforeMsg.uuid } ));

        return { promise: prom, progress }
    },

    clearMsgHash() {
        this.msgHash = {};
    },

    createMsg( msgSpecs ) {
        const msgText = _.escape( msgSpecs.text );

        if( !msgText ) {
            return;
        }

        if( !msgSpecs.type ) {
            msgSpecs.type = 'success';
        }


        if( !this.msgHash[ 'clearPageMsgHash' ] ) { // after a minute with no msg clean up.
            this.msgHash[ 'clearPageMsgHash' ] = _.debounce( () => { this.msgHash = {}; }, 60000 );
        }

        if( !this.msgHash[ msgText ] ) {
            this.msgHash[ msgText ] = _.debounce( msgSpecs => this.debounceCreateMsg( msgSpecs ), 1500, true );
        }

        this.msgHash[ 'clearPageMsgHash' ]();

        return this.msgHash[ msgText ]( msgSpecs );
    },

    removeMsg( msg ) {
        $.notify( 'close', msg );
    },

    ioFailMsg( err, options ) {
        let errorMsg;

        if( err && err.nested ) {
            errorMsg = Utils.collectValidationErrorMsgs( err ).join( ', ' );
        } else {
            errorMsg = err;
        }

        errorMsg = ((options && options.errorDescription) ? (options.errorDescription + '. ') : '') +
                   (errorMsg || ((options && options.defaultError) || 'Unspecified error'));

        if( err.statusText === 'abort' || !errorMsg ) {
            return;
        }

        options = _.extend( { text : errorMsg, type : 'error', width : 900, skipEscape : true }, options || {} );

        return Page.createMsg( options );
    },

    debounceCreateMsg( msgSpecs ) {
        let status = 'default',
            pos    = 'top-center';

        switch( msgSpecs.type ) {
            case 'notify':
                status = 'info';
                break;
            case 'warn':
                status = 'warning';
                break;
            case 'error':
            case 'success':
                status = msgSpecs.type;
                break;
        }

        if( _.isUndefined( msgSpecs.duration ) ) {
            msgSpecs.duration = 3000;
        }

        if( !msgSpecs.skipEscape ) {
            msgSpecs.text = msgSpecs.text.replace( /</g, '&lt;' );
            msgSpecs.text = msgSpecs.text.replace( />/g, '&gt;' );
        }

        let ret = $.notify( {
            message      : msgSpecs.text,
            status       : status,
            timeout      : msgSpecs.duration,
            pos          : msgSpecs && msgSpecs.pos || pos,
            zIndex       : 14500,
            onClose      : msgSpecs.onClose || function() {},
            replace      : msgSpecs.replace,
            closeOnlyOnX : _.isBoolean( msgSpecs.closeOnlyOnX ) ? msgSpecs.closeOnlyOnX : false
        } );

        return ret;
    },
};

export default _.extend( Page, Events );
