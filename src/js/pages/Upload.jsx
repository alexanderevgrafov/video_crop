import React from 'react-mvx';
import {useCallback, useState} from 'react';
import {useDropzone} from 'react-dropzone';

export const UploadPage = ({state}) => {
  const [message, setMessage] = useState();
  const onDrop = useCallback(acceptedFiles => {
    const file = acceptedFiles[0];
    if (file.type.match(/^video\//)) {
      state.mediaFile = file;
    } else {
      setMessage('Файл не распознан как видео');
    }
  }, []);

  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

  return <>
    <div {...getRootProps()} className="upload-zone">
      <input {...getInputProps()} />
      {
        isDragActive ?
          <div>Поместите файл сюда...</div> :
          <div>Нажмите или перетаскивайте сюда новый файл для обработки</div>
      }
      {
        message ? <div>{message}</div> : void 0
      }
    </div>
  </>
};
