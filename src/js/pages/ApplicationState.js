import React from 'react-mvx'
import {Record, type, define} from 'type-r'
import * as socketIOClient from 'socket.io-client';
import Page from 'app/Page'

const server_path = window.location.protocol + '//' + (process.env.WS_SERVER_HOST || 'localhost') + (process.env.WS_USE_PORT_IN_CLIENT ? (':' + process.env.WS_SERVER_PORT) : '');

const server_path2 = window.location.protocol + '//' + (process.env.WS2_SERVER_HOST || 'localhost') + (process.env.WS2_USE_PORT_IN_CLIENT ? (':' + process.env.WS2_SERVER_PORT) : '');

@define
class WsTask extends Record {
  static idAttribute = 'signature';

  static attributes = {
    signature: '',
    resolve: Function,
    reject: Function,
    command: '',
    params: null
  }
}

@define
export class ApplicationState extends Record {
  static attributes = {
    station_name: '',
    server: Record.defaults({
      connected: false,
      reconnect_after: 0,
      reconnect_attempts: 0
    }),
    queue: WsTask.Collection,
    screen: type(String).value('upload'),

    mediaFile: type(File).value(null).watcher('onMediaFileChange'),
    resultFile: '',
    isUploaded: false,
    progress: 0,
    serverStatus: '',
    thumbs: [],
    autoMode: true,

    vlc:null,
  };

  ws = null;
  ws2 = null;
  counter = 0;

  ws_init() {
    const {server} = this;

    this.ws = socketIOClient(server_path);

    this.ws2 = socketIOClient(server_path2);

    this.ws2.on('connect', () => {
    }).on('msg', data => {
        console.log('WS2 message', data);

        this.vlc = data.vlc;
      });


    this.ws.on('connect', () => {
      server.set({
        connected: true,
        reconnect_after: 0,
        reconnect_attempts: 0
      });
    })
      .on('close', () => {
        server.connected = false;
        server.reconnect_attempts++;

        window.setTimeout(() => {
          ws.connect(server_path);
        }, (process.env.WS_RECONNECT_DELAY || 10) * 1000 * server.reconnect_attempts);
      })
      .on('answer', data => {
        let task;
        if (data.__sig && (task = this.queue.get(data.__sig))) {
          const {resolve, reject} = task;

          if (data.__status === 'ok') {
            console.log('Server answer:', data.answer);
            resolve(data.answer);
          } else {
            reject(data.msg || '[no error description provided]');
          }

          this.queue.remove(task);
        } else {
          console.error('WS answer with unknown sig: ', data);
        }
      })
      .on('progress', data => {
        let task;

        if (data.__sig && (task = this.queue.get(data.__sig))) {
          const {command} = task;

          this.reportProgress(command, data.progress.percent);
        } else {
          console.error('WS progress with unknown sig: ', data);
        }
      })
  }

  io2(command) {
    return new Promise((resolve, reject) => {
      this.ws2.emit(command, {ww:22});
    });
  }

  io(command, params = {}) {
    !this.ws && this.ws_init();

    const signature = 'sig' + this.counter++;

    return new Promise((resolve, reject) => {
      this.ws.emit(command, {signature, params});
      this.queue.add({signature, resolve, reject, command, params}, {parse: true});
    });
  }

  reportProgress(command, progress) {
    this.progress = progress;
  }

  resetProgress() {
    this.progress = 0;
    this.serverStatus = '';
  }

  onMediaFileChange() {
    if (this.mediaFile) {
      this.isUploaded = false;
      this.doFileUpload();
      this.goTo('options');
    }
  }

  goTo(where) {
    this.screen = where;
  }

  goToCrop() {
    if (this.autoMode) {
      this.serverStatus = 'Автоматическая обработка...';
      this.resultFile = '';

      this.io('autoProcess', {mode: 'auto'}).then(
        data => {
          this.resultFile = data.resultFile;
          this.resetProgress();
          Page.createMsg({text: 'Обработка завершена'});
        }
      );

      this.goTo('download');

    } else {
      this.serverStatus = 'Создание превью....';
      this.io('getThumbs').then(
        data => {
          this.thumbs = [data.buffer];
          this.resetProgress();
        }
      );

      this.goTo('crop');
    }
  }

  doFileUpload() {
    this.serverStatus = 'Загрузка файла....';

    return new Promise(resolve => {
      const reader = new FileReader();
      const uploadIsDone = () => {
        this.resetProgress();
        Page.createMsg({text: 'Файл загружен'});
        this.isUploaded = true;
        resolve();
      };

      const doUpload = (start = 0, appendTo = '') => {
        const slice_size = 1024 * 1024;
        let next_slice = start + slice_size + 1;
        let blob = this.mediaFile.slice(start, next_slice);

        reader.onloadend = event => {
          if (event.target.readyState !== FileReader.DONE) {
            uploadIsDone();
            return;
          }

          this.io('upload', {
            fileData: event.target.result,
            fileName: this.mediaFile.name,
            appendTo
          }).then(data => {
              this.progress = (start + slice_size) / this.mediaFile.size;

              if (next_slice < this.mediaFile.size) {
                doUpload(next_slice, data.appendTo);
              } else {
                uploadIsDone();
              }
            }
          ).catch(err => Page.createMsg({text: 'Ошибка:' + err, type: 'error'}));
        };

        reader.onerror = () => Page.createMsg({text: 'Произошла ошибка: ' + reader.error, type: 'error'});

        reader.readAsArrayBuffer(blob);
      };

      doUpload(0);
    });
  }

  doCrop(box) {
    this.serverStatus = 'Обрезка....';
    this.resultFile = '';

    this.io('cropProcess', {mode: 'crop', crop: box})
      .then(data => {
        this.resultFile = data.resultFile;
        this.resetProgress();
        Page.createMsg({text: 'Обрезка завершена'});
      });

    this.goTo('download');
  }
}
