import {define} from 'type-r';
import React from 'react-mvx';
import {Button, Row} from 'ui/Bootstrap';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import {ProgressBar} from './Progress'; // see installation section above for versions of NPM older than 3.0.0

const cropper = React.createRef(null);
const cropper_box = React.createRef(null);

@define
export class CropPage extends React.Component {
  onCrop() {
    const [scaleX, scaleY] =[1,1];
    const { x, y, width, height} = cropper.current.getData();
    const box = {left: x * scaleX, top: y * scaleY, width: width * scaleX, height: height * scaleY};

    this.props.state.doCrop(box);
  }

  render() {
    const {state} = this.props;

    return <>
      <Row className="cropper-row">
        {state.thumbs[0] ?
          <div className="cropper-box">
            <div className="cropper-box-padding" ref={cropper_box}>
              <Cropper
                ref={cropper}
                src={'data:image/jpg;base64,' + state.thumbs[0]}
                aspectRatio={process.env.VIDEO_WIDTH / process.env.VIDEO_HEIGHT}
                guides={false}
                zoomable={false}
                movable={false}
                scalable={false}
                rotatble={false}
                background={false}
                resp---onsive={false}
                viewMode={1}
              />
            </div>
          </div>
          : <ProgressBar state={state}/>
        }
      </Row>
      <Row>
        <Button onClick={() => this.onCrop()}>Начать обработку</Button>
      </Row>
    </>;
  }
}

