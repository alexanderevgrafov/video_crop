import React from 'react-mvx';

export const ProgressBar = ({state}) =>
  state.serverStatus ?
    <div className='pbar'>
      <div className='pbar-bar' style={{width:state.progress*100 + '%'}}/>
      <div className='pbar-text'>{ state.serverStatus + Math.round(state.progress*100) + '%'}</div>
    </div> : null;
