import React from 'react-mvx';
import {Button, Row, Col} from 'ui/Bootstrap';
import {ProgressBar} from './Progress'
import cx from 'classnames';

export const OptionsPage = ({state}) => <>
  <Row>
    <Col>
      <div className={cx('process-option', {selected: state.autoMode})} onClick={() => state.autoMode = true}>
        Автоматическая обработка - видео целиком с полями
      </div>
    </Col>
    <Col>
      <div className={cx('process-option', {selected: !state.autoMode})} onClick={() => state.autoMode = false}>
        Выбор области обрезки вручную
      </div>
    </Col>
  </Row>
  <Row>
    {state.isUploaded ?
      <Button onClick={() => state.goToCrop()}>{state.autoMode ? 'Начать обработку' : 'Перейти к обрезке'}</Button>
      : <ProgressBar state={state}/>
    }
  </Row>
</>;
