import React from 'react-mvx';
import { Row } from 'ui/Bootstrap';
import 'cropperjs/dist/cropper.css';
import {ProgressBar} from './Progress';

export const DownloadLast = ({state}) => {
  const {resultFile} = state;
  const resultFileName = resultFile.split('/').pop();

  return resultFile ?
     <a className='link-like' download href={resultFile} target='_blank'>Скачать файл {resultFileName}</a>  : null;
};

export const DownloadPage = ({state}) =>
        <Row>{
            state.resultFile ?
            <DownloadLast state={state}/> :
            <ProgressBar state={state}/>
        }
        </Row>;

