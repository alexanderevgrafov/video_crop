import React from 'react-mvx'
import {define} from 'type-r'
import {Container, Row, Col} from 'ui/Bootstrap'
import {ApplicationState} from './ApplicationState'
import * as platform from 'platform'
import 'scss/app.scss'
import {UploadPage} from './Upload';
import {OptionsPage} from './Options';
import {CropPage} from './Crop';
import {DownloadLast, DownloadPage} from './Download';
import cx from 'classnames';

const LinkLike = (text, page, state) => <div className={cx('link-like', {dis: state.serverStatus})}
                                             onClick={() => state.serverStatus ? null : state.goTo(page)}>{text}</div>;
const ProcessAgain = ({state}) => state.mediaFile ? LinkLike('Обработать файл заново', 'options', state) : null;
const UploadNew = ({state}) => LinkLike('Закачать новый файл', 'upload', state);

const appPages = {
  upload: [UploadPage, ProcessAgain, DownloadLast],
  options: [OptionsPage, UploadNew],
  crop: [CropPage, ProcessAgain, DownloadLast, UploadNew],
  download: [DownloadPage, ProcessAgain, UploadNew],
};

@define
export default class ApplicationPage extends React.Component {
  static state = ApplicationState;

  componentWillMount() {
    const myName = `${platform.name} ${platform.version} ${platform.layout} ${platform.os}`;

    this.state.io('hola', myName);
  }

  render() {
    const {state} = this;
    const {vlc, screen: currentScreen} = state;
    const views = appPages[currentScreen];
    const MainView = views[0];

    return <Container className={currentScreen + '-container'}>
      <div className='main-view'>
        <MainView state={state}/>
      </div>
      <div className='link-views'>
        <Row>{
          views.map((View, index) => index ? <Col key={index}><View state={state}/></Col> : null)
        }
        </Row>
        <Row>
          <Col>
            <button onClick={()=>state.io2('control')}>VLC is { vlc ? 'ON (press to turn off)':'off (press to turn on)' }</button>
          </Col>
        </Row>
      </div>
    </Container>;
  }
}

