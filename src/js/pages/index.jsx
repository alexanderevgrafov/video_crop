import React from 'react-mvx'
import { define } from 'type-r'
import { createBrowserHistory } from 'history'
import {Router, Switch, Route} from 'react-router'
import ApplicationPage from './ApplicationPage'
import 'scss/app.scss'

const history = createBrowserHistory();

@define
export default class Application extends React.Component {
    render() {
        return <Router history={history}>
            <Switch>
                <Route path='/' component={ApplicationPage}/>
            </Switch>
        </Router>;
    }
}
